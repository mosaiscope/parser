var PriorityQueue = require('./lib/queue.js');
var Poll = require('./lib/poll.js');
var neo4j = require('blklab-neo4j');
var moment = require('moment');
var fs = require("fs");

//neo4j.Connect.options.host = process.env.NODE_DB_HOST || '10.223.177.62';
neo4j.Connect.options.host = process.env.NODE_DB_HOST || '104.130.241.94';

neo4j.API.send('MATCH (source:Source)-[s:SUBSCRIBES]-() RETURN COUNT(s) AS cnt, source ORDER BY cnt DESC', {
    params:{},
    ignoreColumns:false
}).then(function(sources){
    console.log(sources.length);
    sources = sources.reverse();
    var i = 0;
    var len = sources.length;
    var next = function(){
        if(i < len){
            var source = sources[i].source;
            console.log('Doing ' + source.title);
            neo4j.API.send('MATCH (a)-[:HASARTICLE]-(s:Source {id:{id}}) REMOVE a:Article SET a:Article RETURN s.title, a.pubDate ORDER BY toInt(a.pubDate) DESC LIMIT 2000', {
                params:{
                    id: source.id
                },
                ignoreColumns:false
            }).then(function(){
                i++;
                setTimeout(function(){
                    next();
                }, 1000);
            });
        }else{
            process.exit(0);
        }
    }
    next();
});
