var Request = require('request');
var Parser = require('./lib/parser.js');
var PriorityQueue = require('./lib/queue.js');
var Poll = require('./lib/poll.js');
var neo4j = require('blklab-neo4j');
var moment = require('moment');
var sanitizeHtml = require('sanitize-html');
var glossary = require("glossary")({ collapse: true });
var striptags = require('striptags');
var uuid = require('uuid');
var stopwords = require('stopwords').english;

neo4j.Connect.options.host = process.env.NODE_DB_HOST || '10.0.0.21';


var articleExists = function(link, callback){
	neo4j.API.send('MATCH (article:Article {link:{link}}) RETURN article', {
		params: {link: link}	
	}).then(function(data){
		if(data.length > 0){
			callback(true);	
		}else{
			console.log(link + ' already exists');
			callback(false);	
		}
	}, function(){
		callback(false);		
	});
}

var update = function(url, params){
	var query;
	var param;
	var len = params.articles.length;
	var article;
	var i = 0;
	var sortedArticles = params.articles.sort(function(a, b){
		if(a.pubDate < b.pubDate) return -1;
		if(a.pubDate > b.pubDate) return 1;
		return 0;
	})
	
	var next = function(i){
		article = sortedArticles[i];
		articleExists(article.link, function(ex){
			if(!ex){
				console.log(article.link);
				param = {
					xmlurl: url,
					article: article
				}
				query = [
					'MATCH (source:Source {xmlurl: { xmlurl }})',
					'WITH source',
					'OPTIONAL MATCH (source)-[r:CURRENT]-(secondlatestupdate)',
					'DELETE r',
					'CREATE (source)-[:CURRENT]->(article:Article { article })',
					'WITH article, collect(secondlatestupdate) AS seconds, source',
					'FOREACH (x IN seconds | CREATE (article)-[:NEXT]->(x))',
					'WITH source, article',
					'SET source.timestamp=timestamp()',
					'RETURN article.title'
				].join('\n');
				
				neo4j.API.send(query, {
					params: param
				}).then(function(ret){
					console.log(ret);
					if(i+1 < len){
						i++
						next(i);	
					}
				}, function(err){
					console.log('error');
					if(i+1 < len){
						i++
						next(i);	
					}
				});
			}
		});
	}
	next(i);
}

var create  = function(params){
	var query = [
		'CREATE (source:Source { source })',
		'WITH source',
		'OPTIONAL MATCH (source)-[r:CURRENT]-(secondlatestupdate)',
		'DELETE r',
		'CREATE (article:Article { articles })',
		'WITH source, COLLECT(article) AS items',		
		'FOREACH(i in RANGE(0, length(items)-2) | ',
		'FOREACH(itemsi in [items[i]] | ',
		'FOREACH(itemsi2 in [items[i+1]] | ',
		'CREATE UNIQUE (itemsi)-[:NEXT]->(itemsi2))))',					
		'WITH source, items[0] AS first',
		'CREATE (source)-[:CURRENT]->(first)',
		'SET source.timestamp=timestamp()',
		'RETURN first.title'
	].join('\n');
	
	neo4j.API.send(query, {
		params: params
	}).then(function(ret){
		console.timeEnd('create');
		if(ret.length > 0){
			console.log('done');
		}else{
			console.log(ret);
		}
	}, function(err){
		console.log(err);
	});
}

function exists(url, callback){
	neo4j.API.send('MATCH (source:Source {xmlurl:{xmlurl}}) RETURN source', {
		params: {xmlurl: url}	
	}).then(function(data){
		if(data.length > 0){
			callback(true);	
		}else{
			callback(false);	
		}
	}, function(){
		callback(false);		
	});
}

Poll.start(
    function() {
		var str;
		var query;
		var params = {
			link:'',
			source:{},
			articles:[]
		}
		
        PriorityQueue.pop(function(url){			
			Parser.parseRSS(url, function(stream){
				var obj = JSON.parse(stream);
				
				if(obj.meta && obj.data && obj.meta.title && obj.data.length > 0){
					console.log(obj.meta + ' - ' + obj.data.length);
					params.link = obj.meta.xmlurl || url;
					
					params.source = {
						xmlurl: obj.meta.xmlurl || url,
						link: obj.meta.link,
						title: obj.meta.title,
						categories: JSON.stringify(obj.meta.categories),
						description: obj.meta.description || '',
						id: uuid.v1()
					}
					
					obj.data.forEach(function(article){			
						var img = /<img[^>]+src="?([^"\s]+)"?[^>]*\/>/g.exec(article.description);
						var articleImg;
						var type = 'half';						
						
						if(img){
							articleImg = '<section class="img" style="background-image:url(' + img[1] + ')"></section>';
							type = 'half';	
						}else{
							articleImg = '';
							type = 'full';
						}
						
						if(article.link && article.title){
							params.articles.push({
								sourceLink: obj.meta.link,
								sourceTitle: obj.meta.title,
								link: article.link,
								origlink: article.origlink || '',
								title: article.title,
								date: new moment(article.date).format('x'),
								pubDate: new moment(article.pubDate).format('x'),
								author: article.author || '',
								source: JSON.stringify(article.source),
								guid: article.guid,
								categories: JSON.stringify(article.categories),
								image: JSON.stringify(article.image),
								summary: sanitizeHtml(article.summary, {
								  allowedTags: [ 'b', 'i', 'em', 'strong', 'a', 'p'],
								  allowedAttributes: {
										'a': [ 'href' ]
									}
								}),
								description: sanitizeHtml(article.description, {
								  allowedTags: [ 'b', 'i', 'em', 'strong', 'a', 'p'],
								  allowedAttributes: {
										'a': [ 'href' ]
									}
								}),
								type: type,
								articleImage: articleImg,
								id: uuid.v1()
							});
						}
					});
					
					console.time('create');

					exists(params.link, function(doesit){
						if(doesit){
							update(params.link, params);	
						}else{
							create(params);	
						}
					});
					
				}
				
			});
		});
    }
);

PriorityQueue.clear();
