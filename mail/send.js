var Mailgun = require('mailgun-js');
var mailgun = new Mailgun({apiKey: 'key-de7f910e5ee2cbc222252fc63545cf59', domain: 'mail.mosaiscope.com'});
var fs = require('fs');

var email = process.argv[2];

try{
    var temp = fs.readFileSync('templates/welcome-clean.html', "utf8");
    var d = {
        from: 'noreply@mail.mosaiscope.com',
        to: email,
        subject: 'Welcome to Mosaiscope',
        html: temp
    }
    mailgun.messages().send(d, function (err, body) {
        if(err){
            console.log(err);
        }else{
            console.log(body);
        }
    });

}catch(e){
    console.log(e);
}
