var neo4j = require('blklab-neo4j');
var moment = require('moment');
var uuid = require('uuid');
var fs = require("fs");
var mongoose = require('mongoose');
var tz = require('moment-timezone');

neo4j.Connect.options.host = process.env.NODE_DB_HOST || '172.99.119.21';

console.log('Connecting to ' + neo4j.Connect.options.host);

mongoose.connect('mongodb://localhost/mosaiscope');

var Stats = mongoose.model('UserStats', {
    total_users: Number,
    total_sources: Number,
    total_topics: Number,
    total_using_sources: Number,
    total_using_topics: Number,
    avg_sources_per_user: Number,
    avg_topics_per_user: Number,
    total_users_who_have_read_article: Number,
    avg_read_per_user: Number,
    total_users_who_have_seen_article: Number,
    total_users_who_have_flagged_article: Number,
    total_users_with_mail: Number,
    total_daily_active: Number,
    rolling_7_day_active: Number,
    rolling_30_day_active: Number,
    date: String,
    sort: Number
});

var today = moment().tz("America/Denver").startOf('day').format('x');

neo4j.API.send("MATCH (u:Users) RETURN COUNT(u) AS totalUsers").then(function(totalUsers){
        neo4j.API.send("MATCH (source:Source)-[:SUBSCRIBES]-(u:Users) RETURN u AS users, COUNT(source) AS total", {ignoreColumns: false}).then(function(totalUsingSources){
            neo4j.API.send("MATCH (source:Topic)-[:SUBSCRIBESTOTOPIC]-(u:Users) RETURN u AS users, COUNT(source) AS total", {ignoreColumns: false}).then(function(totalUsingTopics){
                neo4j.API.send('MATCH (sources:Source) RETURN DISTINCT sources.title').then(function(totalSources){
                    neo4j.API.send('MATCH (:Users)-[s:SUBSCRIBES]-(source:Source) WHERE s.slug <> "untagged" AND s.slug <> "" RETURN COUNT(DISTINCT s.slug)').then(function(totalTopics){
                        neo4j.API.send('MATCH (source:Article)-[:READ {actual:1}]-(u:Users) RETURN u AS users, COUNT(DISTINCT source.title) AS total', {ignoreColumns: false}).then(function(totalRead){
                            neo4j.API.send('MATCH (source:Article)-[:SEEN]-(u:Users) RETURN u AS users, COUNT(source) AS total').then(function(totalSeen){
                                neo4j.API.send('MATCH (source:Article)-[:FLAGGED]-(u:Users) RETURN u AS users, COUNT(source) AS total').then(function(totalFlagged){
                                    neo4j.API.send('MATCH (source:Email)-[:HASMAIL]-(u:Mail) RETURN source AS users, COUNT(u) AS total').then(function(totalMail){
                                        neo4j.API.send('MATCH (u:Users) WHERE toInt(u.last_access) >= ' + today + ' RETURN COUNT(DISTINCT u.email)').then(function(activeToday){
                                            neo4j.API.send('MATCH (u:Users) WHERE toInt(u.last_access) >= 2592000000 RETURN COUNT(DISTINCT u.email)').then(function(active30){
                                                    neo4j.API.send('MATCH (u:Users) WHERE toInt(u.last_access) >= timestamp() - 604800000 RETURN COUNT(DISTINCT u.email)').then(function(active7){

                                                        var filteredSources = {};
                                                        totalSources.forEach(function(row){
                                                            if(!filteredSources[row] && row.indexOf('Comments') == -1){
                                                                filteredSources[row] = 1;
                                                            }
                                                        });

                                                        var totalSourcesCount = 0;
                                                        var totalTopicsCount = 0;
                                                        var totalReadCount = 0;
                                                        var filteredRead = {};

                                                        totalUsingSources.forEach(function(n){
                                                            totalSourcesCount += n.total
                                                        });

                                                        totalUsingTopics.forEach(function(n){
                                                            totalTopicsCount += n.total
                                                        });

                                                        totalRead.forEach(function(n){
                                                            totalReadCount += n.total
                                                        });

                                                        try{
                                                            console.log(Object.keys(filteredSources).length);
                                                        }catch(e){
                                                            console.log(e);
                                                        }

                                                        var stats = new Stats({
                                                            total_users: totalUsers[0],
                                                            total_sources:Object.keys(filteredSources).length,
                                                            total_topics:totalTopics[0],
                                                            total_using_sources:totalUsingSources.length,
                                                            total_using_topics:totalUsingTopics.length,
                                                            avg_sources_per_user: Math.round(totalSourcesCount/totalUsingSources.length),
                                                            avg_topics_per_user: Math.round(totalTopicsCount/totalUsingTopics.length),
                                                            total_users_who_have_read_article:totalRead.length,
                                                            avg_read_per_user: Math.round(totalReadCount/totalRead.length),
                                                            total_users_who_have_seen_article:totalSeen.length,
                                                            total_users_who_have_flagged_article:totalFlagged.length,
                                                            total_users_with_mail:totalMail.length,
                                                            total_daily_active: activeToday[0],
                                                            rolling_7_day_active: active7[0],
                                                            rolling_30_day_active: active30[0],
                                                            date: moment().format("MMM Do YYYY"),
                                                            sort: moment().format("x")
                                                        });
                                                        stats.save(function(err, ret){
                                                            console.log(err);
                                                            console.log(ret);
                                                            process.exit(0);
                                                        })
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});
