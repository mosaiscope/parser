var neo4j = require('blklab-neo4j');
var moment = require('moment');

//neo4j.Connect.options.host = process.env.NODE_DB_HOST || '10.223.177.62';
neo4j.Connect.options.host = process.env.NODE_DB_ENV == 'DEV' ? '10.0.0.51' : '104.130.241.94';

var source_id = process.argv[2];
var corrupt = 0;
var threshold = 86400000;
var x = 2;
var combined = threshold * x;

//Sticky Articles
neo4j.API.send('MATCH (source:Source)-[s:SUBSCRIBES]-() RETURN COUNT(s) AS cnt, source, source.id AS id, source.title AS title ORDER BY cnt DESC', {
    params:{},
    ignoreColumns:false
}).then(function(sources){
    console.log(sources.length);
    var doSource = function(){
        var source = sources.pop();
        if(sources.length > 0){
            var source_id = source.id;
            console.log('Starting ' + source.title);
            neo4j.API.send('MATCH (source:Source {id: {id} })-[:HASARTICLE]-(article:Article) WHERE toInt(article.pubDate) < timestamp() - ' + combined + ' RETURN article.id AS id, article.title AS title ORDER BY toInt(article.pubDate) DESC LIMIT 2000 ', {
                params:{
                    id: source_id
                },
                ignoreColumns:false
            }).then(function(data){
                var len = data.length;
                var corrupt = 0;
                if(len == 2000){
                    console.log('going through ' + len);
                    var check = function(){
                        if(data.length > 0){
                            var row = data.pop();
                            var id = row.id;

                            neo4j.API.send('MATCH (article:Article {id: {id} }) RETURN article.id AS id', {
                                params:{
                                    id: id
                                },
                                ignoreColumns:false
                            }).then(function(d){
                                if(d.length === 0){
                                    corrupt++;
                                     neo4j.API.send('MATCH (article {id: {id} })-[:HASARTICLE]-(:Source {id: {source_id} }) DETACH DELETE article', {
                                        params:{
                                            id: id,
                                            source_id: source_id
                                        },
                                        ignoreColumns:false
                                    }).then(function(){
                                        check();
                                    });
                                }else{
                                    check();
                                }
                            });
                        }else{
                            console.log(source.title + ' had ' + corrupt + ' out of ' + len + ' corrupted');
                            doSource();
                        }

                    };

                    check();
                }else{
                    doSource();
                }
            });
        }else{
            process.exit();
        }
    };
    doSource();
});



//Sticky Emails

/*neo4j.API.send('MATCH (email:Email)-[:HASMAIL]-(msg:Mail) RETURN msg.id AS id, msg.title AS title', {
    params:{},
    ignoreColumns:false
}).then(function(data){
    var len = data.length;
    var corrupt = 0;
    console.log('checking ' + len);

    var check = function(){
        if(data.length > 0){
            var row = data.pop();
            var id = row.id;
            console.log(id);
            neo4j.API.send('MATCH (email:Email)-[:HASMAIL]-(article {id: {id} }) RETURN article.id AS id', {
                params:{
                    id: id
                },
                ignoreColumns:false
            }).then(function(d){
                if(d.length === 0){
                    corrupt++;
                    check();
                     neo4j.API.send('MATCH (article {id: {id} })-[:HASMAIL]-(:Email {email: "rokos75751@mail.mosaiscope.com"}) DETACH DELETE article', {
                        params:{
                            id: id,
                            source_id: source_id
                        },
                        ignoreColumns:false
                    }).then(function(){
                        check();
                    });
                }else{
                    check();
                }
            });
        }else{
            console.log('had ' + corrupt + ' out of ' + len + ' corrupted');
            process.exit();
        }

    };

    check();
});
*/
