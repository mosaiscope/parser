var PriorityQueue = require('./lib/queue.js');
var Poll = require('./lib/poll.js');
var neo4j = require('blklab-neo4j');
var moment = require('moment');

neo4j.Connect.options.host = process.env.NODE_DB_ENV == 'DEV' ? '10.0.0.51' : '10.223.177.62';

console.log('Connecting to ' + neo4j.Connect.options.host);

//http://www.jalopnik.com/index.xml
neo4j.API.send('MATCH (source:Source) WITH DISTINCT source.xmlurl AS url OPTIONAL MATCH (s2:Source {xmlurl:url}) RETURN url, COUNT(s2) AS cnt ORDER BY cnt DESC', {ignoreColumns:false}).then(function(data){
	//data.forEach(function(source){
		var i = 0;
		//var data = [{url:"http://www.avclub.com/feed/rss/", cnt:73}]
	
		var next = function(source){
			console.log(i + ' of ' + data.length);
			
			if(source.cnt > 1 && i < data.length){
				var q = 'MATCH (n:Source) WHERE n.xmlurl = {url} AND (n)-[:SUBSCRIBES|ISTOPIC]-() RETURN COUNT(n) AS cnt';
				neo4j.API.send(q, {
					params:{
						url: source.url
					}, ignoreColumns:false}).then(function(data2){

					data2.forEach(function(s2){

						if(s2.cnt > 0){
							console.log(source.url + " Has Sub")
							var q2 = 'MATCH (n:Source) WHERE n.xmlurl = {url} AND NOT (n)-[:SUBSCRIBES]-() AND NOT (n)-[:ISTOPIC]-() DETACH DELETE n RETURN COUNT(*)';
							neo4j.API.send(q2, {
							params:{
								url: source.url
							}, ignoreColumns:false}).then(function(d){
								console.log(d);
								i++;
								next(data[i]);
							})
						}else{
							console.log(source.url + " No Sub")
							var q2 = "MATCH (c:Source) WHERE c.xmlurl = {url} WITH c.xmlurl as xmlurl, collect(c) as urls, count(*) as cnt WHERE cnt > 1 WITH head(urls) as first, tail(urls) as rest UNWIND rest AS to_delete DETACH DELETE to_delete RETURN count(*)"
							neo4j.API.send(q2, {
							params:{
								url: source.url
							}, ignoreColumns:false}).then(function(d){
								console.log(d);
								i++;
								next(data[i]);
							})
						}
					})
				});
			}	
		}
		next(data[i]);
	//})	
	/*var chunk = 500;
	var start = 0;
	var end = start + chunk;
	var done = 0;
	var len = data.length
	data.forEach(function(id){
		var query = [
			'MATCH (s:Source {id:{id}})-[:CURRENT|NEXT*501..1000]-(a:Article)',
			'CREATE UNIQUE s-[:HASARTICLE]-a',
			'RETURN null'
		].join('\n');
		neo4j.API.send(query, {params:{
			id: id
		}}).then(function(data){
			done++;
			console.log(done + '/' + len); 
		});
	});*/
});