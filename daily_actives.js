var neo4j = require('blklab-neo4j');
var moment = require('moment');
var tz = require('moment-timezone');

neo4j.Connect.options.host = '104.130.241.94';

console.log(moment().tz("America/Denver").startOf('day').format('x'));

neo4j.API.send('MATCH (u:Users) WHERE toInt(u.last_access) >= ' + moment().tz("America/Denver").startOf('day').format('x') + ' RETURN COUNT(DISTINCT u.email)').then(function(active7){
    console.log(active7);
    process.exit(0);
});