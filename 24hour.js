var Mailgun = require('mailgun-js');
var mailgun = new Mailgun({apiKey: 'key-de7f910e5ee2cbc222252fc63545cf59', domain: 'mail.mosaiscope.com'});
var fs = require('fs');
var neo4j = require('blklab-neo4j');
var moment = require('moment');

var dev = "172.99.119.21";
var threshold = (moment("2016-11-02T00:00:00.000Z").format('x')/1000);
neo4j.Connect.options.host = dev;

var App = {
    users: [],
    len: 0,

    fetch: function(){
        neo4j.API.send("MATCH (user:Users) WHERE NOT (user)-[:SUBSCRIBES]-() AND NOT (user)-[:SUBSCRIBESTOTOPIC]-() AND NOT EXISTS(user.sent24) RETURN user.id AS id, user.email AS email, user.created AS created", {ignoreColumns:false}).then(function(users){
            if(users.length > 0){
                users.forEach(function(user){
                    var n = moment();
                    var l = moment(user.created);
                    var diff = n.diff(l, 'hours');
                    console.log(user.id + ' - ' + diff);
                    if(diff >= 22){
                        App.users.push(user);
                    }
                });
                if(App.users.length > 0){
                    console.log('processing');
                    App.len = App.users.length;
                    App.process();
                }else{
                    console.log('no users');
                    process.exit(0);
                }
            }
        });
    },

    process: function(){
        if(App.users.length > 0){
            var user = App.users.pop();
            neo4j.API.send("MATCH (user:Users {id:{id}}) SET user.sent24=1 RETURN user", {params:{id: user.id}, ignoreColumns:false}).then(function(users){
                try{
                    var temp = fs.readFileSync('/var/www/parser/mail/templates/24-hour-clean.html', "utf8");
                    var d = {
                        from: 'noreply@mail.mosaiscope.com',
                        to: user.email,
                        subject: 'Mosaiscope™ Help: Adding Content',
                        html: temp
                    };
                    console.log(d);
                    mailgun.messages().send(d, function (err, body) {
                        console.log('sent');
                        App.process();
                    });
                }catch(e){
                    console.log(e);
                    App.process();
                }
            }, function(err){
                console.log('err');
                console.log(err);
                App.process();
            });
        }else{
            var d = {
                from: 'noreply@mail.mosaiscope.com',
                to: "baird@lackner-buckingham.com",
                subject: 'Processed ' + App.len + ' Users',
                html: "Processed users"
            };
            mailgun.messages().send(d, function (err, body) {
                console.log('done processing users');
                process.exit(0);
            });

        }
    }
};


App.fetch();

