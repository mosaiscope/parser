var PriorityQueue = require('./lib/queue.js');
var Poll = require('./lib/poll.js');
var neo4j = require('blklab-neo4j');
var moment = require('moment');

//neo4j.Connect.options.host = process.env.NODE_DB_HOST || '10.223.177.62';
neo4j.Connect.options.host = process.env.NODE_DB_ENV == 'DEV' ? '10.0.0.51' : '172.99.119.21';

console.log(neo4j.Connect.options.host);

var running = false;
var threshold = 600;
var sources = {};
var unusedSources = {};
var lastPopulated = 0;
var first = true;
var start = 1;

var ready = function(previous, threshold) {
    if(!first){
        var elapsed = new moment().diff(previous, 'minutes');
        if(elapsed < threshold) {
             return false;
        }else{
            return true;
        }
    }else{
        return true;
    }
};

var shouldPopulate = function() {
    if(lastPopulated > 0){
        var elapsed = new moment().diff(lastPopulated, 'minutes');
        console.log('sources updated ' + new moment(lastPopulated).fromNow());
        if(elapsed < 60) {
             return false;
        }else{
            return true;
        }
    }else{
        return true;
    }
};

function run(){
    console.log('running...');
    start += 1;
    Object.keys(sources).forEach(function(url){
        var source = sources[url];
        if(ready(source.stamp, 5)){
            source.stamp = new moment();
            //var w = 2 - (1 * (parseFloat(source.cnt * 1.0)/100.0));
            var w = start;
            //PriorityQueue.remove(url);
            PriorityQueue.push(url, w);
        }
    });
    first = false;
}

function populate(){
    console.log('populating...');
    neo4j.API.send('MATCH (source:Source)-[s:SUBSCRIBES]-(u: Users) WHERE (toInt(u.last_access) >= timestamp() - (1000*60*60*168)) RETURN DISTINCT source.id AS id', {ignoreColumns:false}).then(function(used){
            sources = {};
            used.forEach(function(source){
                sources[source.id + ':-:true'] = {
                    stamp: new moment(),
                    cnt: 0
                };
            });

            lastPopulated = new moment();

            run();
    });
}

PriorityQueue.clear();

Poll.start(
    function() {
        if(shouldPopulate()){
            populate();
        }else{
            run();
        }
    }, 60000
);
