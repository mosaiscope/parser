var FeedParser = require('feedparser');
var request = require('request');
var cheerio = require('cheerio');
var url = require('url');

function call(url, callback){
    url = url.replace(/([^:]\/)\/+/g, "$1");
    if(!callback){
        var req = request(url, {timeout: 500000, pool: false});
        req.setMaxListeners(50);
        req.setHeader('user-agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36');
        req.setHeader('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8');
        req.setHeader('Accept-Charset', 'ISO-8859-1,utf-8;q=0.7,*;q=0.3');
        req.setHeader('Accept-Encoding', 'none');
        req.setHeader('Accept-Language', 'en-US,en;q=0.8');
        req.setHeader('Connection', 'keep-alive');
        req.on('error', function (error) {
           console.log(error);
        });
        return req;
    }else{
        var options = {
          url: url,
          followAllRedirects: true,
          headers: {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
            'Accept-Encoding': 'none',
            'Accept-Language': 'en-US,en;q=0.8',
            'Connection': 'keep-alive'
          }
        };
        var req = request(options, callback);
        req.on('error', function (error) {
           console.log(error);
        });
    }
}

function parseRSSMeta(link, body, callback){
    var $ = cheerio.load(body);
    var link;
    var data = {
        links: [],
        icons: [],
        meta: {},
        exists: false
    }

    data.meta.title = $('title').first().text();
    var icon = $('icon').first().text();
    if(icon){
        data.icons.push({url: icon});
    }
    data.links.push({
        type: '',
        url: link,
        title: data.meta.title || link
    });

    callback.call(this, data);
}

function parseLinkMeta(link, body, callback){
    var $ = cheerio.load(body);

    var img = "";
    var meta = $('meta');
    meta.each(function(i, m){
        if(m.attribs.name == "og:image" || m.attribs.property == "og:image"){
            img = '<section class="img" style="background-image:url(' + m.attribs.content + ')"></section>';
        }

        if(img == "" && (m.attribs.name == "twitter:image:src" || m.attribs.property == "twitter:image")){
            img = '<section class="img" style="background-image:url(' + m.attribs.content + ')"></section>';
        }
    });

    callback.call(this, img);
}

function checkForRSS(body, callback){
    var $ = cheerio.load(body);
    var link;
    var data = {
        links: [],
        icons: [],
        meta: {},
        exists: false
    };

    var hrefs = [];

    $('head').children().each(function(idx,elem){
        link = $(elem);
        console.log('checking for rss in ' + link);
        if(link.attr('rel') == 'alternate' && link.attr('type').indexOf('xml') != -1){
            if(hrefs.indexOf(link.attr('href')) == -1){
                data.links.push({
                    type: link.attr('type'),
                    url: link.attr('href'),
                    title: link.attr('title') || link.attr('href')
                });
                hrefs.push(link.attr('href'));
            }
        }else if(link.attr('rel') && link.attr('href') && link.attr('rel').indexOf('icon') != -1 && link.attr('href').indexOf('.png') != -1){
            data.icons.push({
                type: link.attr('type'),
                url: link.attr('href')
            });
        }else if(elem.tagName == 'title'){
            data.meta.title = link.text();
        }
    });

    callback.call(this, data);
}

var internals = {};

exports = module.exports = internals.Web = {

    fetch: function(url, callback){
        try{
            call(url, function(err, res, body){
                if(err){
                    return callback.call(this, {links:[], icons:[]});
                }
                if(res.statusCode != 200){
                    return callback.call(this, {links:[], icons:[]});
                }

                var contentType = res.headers['content-type'];

                var $ = cheerio.load(body);

                if(contentType.indexOf('xml') != -1 || $('rss').length > 0 || $('feed').length){
                    parseRSSMeta(url, body, function(feeds){
                        callback.call(this, feeds);
                    });
                }else if(contentType.indexOf('html') != -1){
                    checkForRSS(body, function(feeds){
                        callback.call(this, feeds);
                    });
                }else{
                    callback.call(this, {links:[], icons:[]});
                }
            });
        }catch(e){}
    },

    parseRSS: function (feed, callback){
        try{
            var feedparser = new FeedParser();
            var data = {
                meta:{},
                data:[]
            }

            var req = call(feed);
            req.on('error', function (error) {
                console.log('Request Error:');
                console.log(error.stack);
                callback.call(this, JSON.stringify(error));
            });
            req.on('response', function (res) {
                var stream = this;
                if (res.statusCode != 200){
                    console.log('Bad Status Code');
                }
                stream.pipe(feedparser);
            });


            feedparser.on('error', function(error) {
                console.log('Parse Error:');
                console.log(error);
                callback.call(this, JSON.stringify(error));
            });
            feedparser.on('end', function() {
                callback.call(this, JSON.stringify(data));
            });
            feedparser.on('readable', function() {
                var stream = this;
                var item;
                data.meta = this.meta;
                while (item = stream.read()) {
                    data.data.push(item);
                }
            });
        }catch(e){
            console.log(e);
        }
    },

    parseJSON: function (feed, callback){
        try{
            var data = {
                meta:{},
                data:[]
            }

            var req = call(feed, function(error, response, body){
                callback.call(this, body)
            });
        }catch(e){
            console.log(e);
        }
    },

    checkForArticleImage: function(url, callback){
        try{
            call(url, function(err, res, body){
                if(err){
                    console.log('Error fetching image');
                    console.log(err);
                    callback.call(this, "");
                }else{
                    parseLinkMeta(url, body, function(img){
                        callback.call(this, img);
                    });
                }
            });
        }catch(w){
            callback.call(null, "");
        }
    }
};
