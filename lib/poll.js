function poll(fn, interval) {
    interval = interval || 1000;

    (function p() {
		if(fn()) {
			callback();
		}else{
			setTimeout(p, interval);
		}
    })();
}

var internals = {};

exports = module.exports = internals.Poll = {
	start: poll
};