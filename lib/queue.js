var Redis = require('redis');
var Client = Redis.createClient()

//console.log(moment('Fri, 26 Jun 2015 18:46:30 GMT').format('x'));

var key = "mosaiscope:sources";

function pop(callback){
    get_members(function(err, members){
        members.forEach(function(member){
            remove(member, function(){
                callback.call(this, member);
            });
        });
    });
}

function add(member, score, _key){
	var k = _key ? _key : key
    Client.zadd([k, score, member], function(err, response) {
        if(err){
            console.log(err);
        }
    })
}

function remove(member, callback, _key){
	var k = _key ? _key : key
    Client.zrem([k, member], function(err, response) {
        if(err){
            console.log(err);
        }else{
            if(callback){
                callback.call(this, response);
            }
        }
    })
}

function clear(_key){
	var k = _key ? _key : key
    Client.del(k);
}

function get_members(callback, _key){
	var k = _key ? _key : key
    Client.zrange(k, 0, 0, function(err, response){
        callback.call(this, err, response);
    });
}

function get_score(member, callback, _key){
	var k = _key ? _key : key
    Client.zscore([k, member], function(err, response){
        if(callback){
            callback.call(this, response);
        }
    });
}


var internals = {};

exports = module.exports = internals.PriorityQueue = {
    push: add,
    remove: remove,
    pop: pop,
    key: key,
    clear: clear
};
