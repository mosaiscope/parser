var Request = require('request');
var Parser = require('./lib/parser.js');
var PriorityQueue = require('./lib/queue.js');
var Poll = require('./lib/poll.js');
var neo4j = require('blklab-neo4j');
var moment = require('moment');
var sanitizeHtml = require('sanitize-html');
var glossary = require("glossary")({ collapse: true });
var striptags = require('striptags');
var resanitize = require('resanitize');
var uuid = require('uuid');
var stopwords = require('stopwords').english;
//var memwatch = require('memwatch');
var normalizeUrl = require('normalize-url');

//neo4j.Connect.options.host = process.env.NODE_DB_HOST || '10.223.177.62';
neo4j.Connect.options.host = process.env.NODE_DB_ENV == 'DEV' ? '10.0.0.51' : '172.99.119.21';

function stripAds (str) {
  return str.replace(/<div[^>]*?class=("|')snap_preview\1[^>]*?>(?:<br[^>]*?>)?([\s\S]*?)<\/div>/gi, '$2')
            .replace(/<div[^>]*?class=("|')(?:feedflare|zemanta-pixie)\1[^>]*?>[\s\S]*?<\/div>/gi, '')
            .replace(/<!--AD BEGIN-->[\s\S]*?<!--AD END-->\s*/gi, '')
            .replace(/<table[^>]*?>[\s\S]*?<\/table>\s*?<div[^>]*?>[\s\S]*?Ads by Pheedo[\s\S]*?<\/div>/gi, '')
            .replace(/<table[^>]*?>.*?<img[^>]*?src=("|')[^>]*?advertisement[^>]*?\1.*?>.*?<\/table>/gi, '')
            .replace(/<br[^>]*?>\s*?<br[^>]*?>\s*?<span[^>]*?class=("|')advertisement\1[^>]*?>[\s\S]*?<\/span>[\s\S]*<div[^>]*?>[\s\S]*?Ads by Pheedo[\s\S]*?<\/div>/gi, '')
            .replace(/<br[^>]*?>\s*?<br[^>]*?>\s*?(?:<[^>]+>\s*)*?<hr[^>]*?>\s*?<div[^>]*?>(?:Featured Advertiser|Presented By:)<\/div>[\s\S]*<div[^>]*?>[\s\S]*?(?:Ads by Pheedo|ads\.pheedo\.com)[\s\S]*?<\/div>/gi, '')
            .replace(/<br[^>]*?>\s*?<br[^>]*?>\s*?<a[^>]*?href=("|')http:\/\/[^>]*?\.?(?:pheedo|pheedcontent)\.com\/[^>]*?\1[\s\S]*?<\/a>[\s\S]*$/gim, '')
            .replace(/<div[^>]*?class=("|')cbw snap_nopreview\1[^>]*?>[\s\S]*$/gim, '')
            .replace(/<div><a href=(?:"|')http:\/\/d\.techcrunch\.com\/ck\.php[\s\S]*?<\/div> */gi, '')
            .replace(/<(p|div)[^>]*?>\s*?<a[^>]*?href=("|')[^>]+?\2[^>]*?><img[^>]*?src=("|')http:\/\/(?:feedads\.googleadservices|feedproxy\.google|feeds2?\.feedburner)\.com\/(?:~|%7e)[^>]*?\/[^>]+?\3[^>]*?>[\s\S]*?<\/\1>/gi, '')
            .replace(/<(p|div)[^>]*?>\s*?<a[^>]*?href=("|')[^>]+?\2[^>]*?><img[^>]*?src=("|')http:\/\/feeds\.[^>]+?\.[^>]+?\/(?:~|%7e)[^>]\/[^>]+?\3[^>]*?>[\s\S]*?<\/\1>/gi, '')
            .replace(/<a[^>]*?href=("|')http:\/\/feeds\.[^>]+?\.[^>]+?\/(?:~|%7e)[a-qs-z]\/[^>]+?\1[\s\S]*?<\/a>/gi, '')
            .replace(/<a[^>]*?href=("|')http:\/\/[^>]*?\.?addtoany\.com\/[^>]*?\1[\s\S]*?<\/a>/gi, '')
            .replace(/<a[^>]*?href=("|')http:\/\/feeds\.wordpress\.com\/[\.\d]+?\/(?:comments|go[\s\S]*)\/[^>]+?\1[\s\S]*?<\/a> ?/gi, '')
            .replace(/<a[^>]*?href=("|')http:\/\/[^>]*?\.?doubleclick\.net\/[^>]*?\1[\s\S]*?<\/a>/gi, '')
            .replace(/<a[^>]*?href=("|')http:\/\/[^>]*?\.?fmpub\.net\/adserver\/[^>]*?\1[\s\S]*?<\/a>/gi, '')
            .replace(/<a[^>]*?href=("|')http:\/\/[^>]*?\.?eyewonderlabs\.com\/[^>]*?\1[\s\S]*?<\/a>/gi, '')
            .replace(/<a[^>]*?href=("|')http:\/\/[^>]*?\.?pheedo\.com\/[^>]*?\1[\s\S]*?<\/a>/gi, '')
            .replace(/<a[^>]*?href=("|')http:\/\/api\.tweetmeme\.com\/share\?[^>]*?\1[\s\S]*?<\/a>/gi, '')
            .replace(/<p><a[^>]*?href=("|')http:\/\/rss\.cnn\.com\/+?(?:~|%7e)a\/[^>]*?\1[\s\S]*?<\/p>/gi, '')
            .replace(/<img[^>]*?src=("|')http:\/\/feeds\.[^>]+?\.[^>]+?\/(?:~|%7e)r\/[^>]+?\1[\s\S]*?>/gi, '')
            .replace(/<img[^>]*?src=("|')http:\/\/rss\.nytimes\.com\/c\/[^>]*?\1.*?>.*$/gim, '')
            .replace(/<img[^>]*?src=("|')http:\/\/feeds\.washingtonpost\.com\/c\/[^>]*?\1.*?>.*$/gim, '')
            .replace(/<img[^>]*?src=("|')http:\/\/[^>]*?\.?feedsportal\.com\/c\/[^>]*?\1.*?>.*$/gim, '')
            .replace(/<img[^>]*?src=("|')http:\/\/(?:feedads\.googleadservices|feedproxy\.google|feeds2\.feedburner)\.com\/(?:~|%7e)r\/[^>]+?\1[\s\S]*?>/gi, '')
            .replace(/<img[^>]*?src=("|')http:\/\/rss\.cnn\.com\/~r\/[^>]*?\1[\s\S]*?>/gi, '')
            .replace(/<img[^>]*?src=("|')http:\/\/[^>]*?\.?fmpub\.net\/adserver\/[^>]*?\1[\s\S]*?>/gi, '')
            .replace(/<img[^>]*?src=("|')http:\/\/[^>]*?\.?pheedo\.com\/[^>]*?\1[\s\S]*?>/gi, '')
            .replace(/<img[^>]*?src=("|')http:\/\/stats\.wordpress\.com\/[\w]\.gif\?[^>]*?\1[\s\S]*?>/gi, '')
            .replace(/<img[^>]*?src=("|')http:\/\/feeds\.wired\.com\/c\/[^>]*?\1.*?>.*$/gim, '')
            .replace(/<p><strong><em>Crunch Network[\s\S]*?<\/p>/gi, '')
            .replace(/<embed[^>]*?castfire_player[\s\S]*?> *?(<\/embed>)?/gi, '')
            .replace(/<embed[^>]*?src=("|')[^>]*?castfire\.com[^>]+?\1[\s\S]*?> *?(<\/embed>)?/gi, '')
            .replace(/<p align=("|')right\1><em>Sponsor<\/em>[\s\S]*?<\/p>/gi, '')
            .replace(/<div [\s\S]*?<img [^>]*?src=(?:"|')[^>]*?\/share-buttons\/[\s\S]*?<\/div>[\s]*/gi, '')
            // This is that annoying footer in every delicious item
            .replace(/<span[^>]*?>\s*?<a[^>]*?href=("|')[^\1]+?src=feed_newsgator\1[^>]*?>[\s\S]*<\/span>/gi, '')
            // This is the annoying footer from ATL
            .replace(/<p[^>]*?><strong><a[^>]*?href=("|')[^>]*?abovethelaw\.com\/[\s\S]+?\1[^>]*?>Continue reading[\s\S]*<\/p>/gi, '')
            // This is the annoying link at the end of WaPo articles
            .replace(/<a[^>]*?>Read full article[\s\S]*?<\/a>/gi, '')
            // These ads go...
            .replace(/<div[^>]*?><a[^>]*?href=("|')[^>]*?crunchbase\.com\/company\/[\s\S]+?\1[^>]*?>[\s\S]*?<div[\s\S]*?>Loading information about[\s\S]*?<\/div>/gi, '')
            .replace(/<div[^>]*?class=("|')cb_widget_[^>]+?\1[\s\S]*?><\/div>/gi, '')
            .replace(/<div[^>]*?class=("|')cb_widget_[^>]+?\1[\s\S]*?>[\s\S]*?<\/div>/gi, '')
            // Before these
            .replace(/<a[^>]*?href=("|')[^>]*?crunchbase\.com\/\1[\s\S]*?<\/a>\s*/gi, '')
            .replace(/<div[^>]*?class=("|')cb_widget\1[^>]*?>[\s\S]*?<\/div>/gi, '')
            // Clean up some empty things
            //.replace(/<(div|p|span)[^>]*?>(\s|<br *?\/?>|(?R))*?<\/\1>/gi, '')
            .replace(/(\s|<br[^>]*?\/?>)*$/gim, '')
            .replace(/<img.*?feedsportal.*?>.*$/gim, '')
            .replace(/<img.*?pixel\.wp\.com.*?>.*$/gim, '');
}

var articleExists = function(link, guid, source_id, callback){
    /*console.log("checking");
    console.log(guid);
    console.log(source_id);
    console.log("");*/
    neo4j.API.send('MATCH (article:Article {guid: {guid} }) RETURN article.id LIMIT 1', {
        params: {guid:guid, source_id: source_id}
    }).then(function(data){
        if(data.length > 0){
            callback(true, data[0].id);
        }else{
            //console.log(link + ' added');
            callback(false);
        }
    }, function(err){
        //console.log(err);
        callback(false);
    });
}

var articleExistsWithSource = function(link, guid, source_id, callback){
    /*console.log("checking");
    console.log(guid);
    console.log(source_id);
    console.log("");*/
    neo4j.API.send('MATCH (article:Article {guid: {guid} })-[:HASARTICLE]-(:Source {id: {source_id} }) RETURN article.id LIMIT 1', {
        params: {guid:guid, source_id: source_id}
    }).then(function(data){
        if(data.length > 0){
            callback(true, data[0].id);
        }else{
            //console.log(link + ' added');
            callback(false);
        }
    }, function(err){
        //console.log(err);
        callback(false);
    });
}

var update = function(url, id, params){
    var query;
    var param;
    var len = params.articles.length;
    var article;
    var i = 0;
    var sortedArticles = params.articles.sort(function(a, b){
        if(a.pubDate < b.pubDate) return -1;
        if(a.pubDate > b.pubDate) return 1;
        return 0;
    });

    var next = function(i){
        article = sortedArticles[i];
        //console.log('checking ' + article.link);
            articleExists(article.link, (article.guid || ''), id, function(ex){
                if(!ex){
                    console.log('creating ' + article.title);
                    //console.log(article.link);
                    var complete = function(){
                        param = {
                            id: id,
                            xmlurl: url,
                            article: article
                        };

                        query = [
                            'MATCH (source:Source {id: { id }})',
                            'WITH source',
                            'OPTIONAL MATCH (source)-[r:CURRENT]-(secondlatestupdate)',
                            'DELETE r',
                            'CREATE (source)-[:CURRENT]->(article:Article { article })',
                            'CREATE UNIQUE (source)-[:HASARTICLE]->(article)',
                            'WITH source, article',
                            'SET source.timestamp=timestamp(), source.count = source.count + 1',
                            'RETURN article.title'
                        ].join('\n');

                        neo4j.API.send(query, {
                            params: param
                        }).then(function(ret){
                            if(i+1 < len){
                                i++;
                                next(i);
                            }
                        }, function(err){
                            console.log(err);
                            if(i+1 < len){
                                i++;
                                next(i);
                            }
                        });
                    };


                if(!article.articleImage){
                    if(article.link.indexOf('.mp3') == -1){
                        Parser.checkForArticleImage(article.link, function(img){
                            article.articleImage = img;
                            complete();
                        });
                    }else{
                        complete();
                    }
                }else{
                    complete();
                }

            }else{
                articleExistsWithSource(article.link, (article.guid || ''), id, function(ex){
                    if(!ex){
                        console.log('attaching ' + article.title);
                        var complete = function(){
                            param = {
                                id: id,
                                xmlurl: url,
                                article_id: article.id
                            };

                            query = [
                                'MATCH (source:Source {id: { id }})',
                                'MATCH (article:Article {id: {article_id} })',
                                'CREATE UNIQUE (source)-[:HASARTICLE]-(article)',
                                'WITH source, article',
                                'SET source.timestamp=timestamp(), source.count = source.count + 1',
                                'RETURN article.title'
                            ].join('\n');

                            neo4j.API.send(query, {
                                params: param
                            }).then(function(ret){
                                if(i+1 < len){
                                    i++;
                                    next(i);
                                }
                            }, function(err){
                                if(i+1 < len){
                                    i++;
                                    next(i);
                                }
                            });
                        };

                        complete();
                    }else{
                        if(i+1 < len){
                            i++;
                            next(i);
                        }
                    }
                });
            }
        });
    }
    next(i);
}

var create  = function(params){
    //console.log('creating');

    var len = params.articles.length;
    var i = 0;

    var next = function(){
        //console.log(i + ' of ' + len);
        if(i < len){
            var article = params.articles[i];

            if(!article.articleImage){
                if(article.link.indexOf('.mp3') == -1){
                    Parser.checkForArticleImage(article.link, function(img){
                        params.articles[i].articleImage = img;
                        i++;
                        next();
                    });
                }else{
                    i++;
                    next();
                }
            }else{
                i++;
                next();
            }
        }else{
            var query = [
                'CREATE (source:Source { source })',
                'WITH source',
                'OPTIONAL MATCH (source)-[r:CURRENT]-(secondlatestupdate)',
                'DELETE r',
                'CREATE (article:Article { articles })',
                'WITH source, COLLECT(article) AS items',
                'FOREACH(i in RANGE(0, length(items)-2) | ',
                'FOREACH(itemsi in [items[i]] | ',
                'FOREACH(itemsi2 in [items[i+1]] | ',
                'CREATE UNIQUE (itemsi)-[:NEXT]->(itemsi2)',
                'CREATE UNIQUE (source)-[:HASARTICLE]->(itemsi))))',
                'WITH source, items[0] AS first',
                'CREATE (source)-[:CURRENT]->(first)',
                'CREATE UNIQUE (source)-[:HASARTICLE]->(first)',
                'SET source.timestamp=timestamp(), source.count = source.count + 1',
                'RETURN first.title'
            ].join('\n');

            neo4j.API.send(query, {
                params: params
            }).then(function(ret){
                //console.log(ret);
                //console.timeEnd('create');
                if(ret.length > 0){
                    //console.log('done');
                }else{
                    //console.log(params.source.xmlurl + ' Didn\'t Add');
                }
            }, function(err){
                console.log(err);
            });
        }
    };
    next();
};

function exists(url, callback){
    url = stripTrailingSlash(url.replace('http://', '').replace('https://', ''));
    //console.log(url);
    neo4j.API.send('MATCH (source:Source) WHERE source.xmlurl =~ {xmlurl} RETURN source', {
        params: {xmlurl: '.*' + url.replace('?', '\\?') + '.*'}
    }).then(function(data){
        if(data.length > 0){
            var id = data[0].id;
            callback(true, id);
        }else{
            callback(false);
        }
    }, function(err){
        console.log(err);
        callback(false);
    });
}

function stripTrailingSlash(str) {
    if(str.substr(-1) === '/') {
        return str.substr(0, str.length - 1);
    }
    return str;
}

/*Poll.start(
    function() {
        PriorityQueue.pop(function(url){
            //console.log('starting');
            var str;
            var query;
            var params = {
                link:'',
                source:{},
                articles:[]
            }
            var c = url.split(':-:');
            url = c[0];
            var isID = c[1];
            if(isID){
                console.log(url);
                neo4j.API.send('MATCH (source:Source {id:{id}}) RETURN source', {
                    params: {id: url}
                }).then(function(data){
                    if(data.length > 0){
                        if(data[0].jsonurl){

                        } else if(data[0].xmlurl){
                            Parser.parseRSS(data[0].xmlurl, function(stream){
                                var obj = JSON.parse(stream);
                                if(obj.meta && obj.data && obj.data.length > 0){
                                    params.link = (obj.meta.xmlurl || url);
                                    var link = (obj.meta.link || url);
                                    console.log(data[0].title);
                                    params.source = data[0];
                                    var links = [];
                                    obj.data.forEach(function(article){
                                        var descrip = article.description ? stripAds(article.description) : '';

                                        var img = /<img[^>]+src="?([^"\s]+)"?[^>]*\/>/g.exec(descrip);
                                        var articleImg;
                                        var type = 'half';

                                        if(img){
                                            articleImg = '<section class="img" style="background-image:url(' + img[1] + ')"></section>';
                                            type = 'half';
                                        }else{
                                            articleImg = '';
                                            type = 'full';
                                        }

                                        if(article.link && article.title){
                                            var pdate = new moment(article.pubDate);
                                            if(!pdate.isValid() || pdate.format('x') > Date.now()){
                                                pdate = new moment().format('x');
                                            }else{
                                                pdate = pdate.format('x');
                                            }

                                            if(links.indexOf(article.link) == -1){
                                                params.articles.push({
                                                    sourceLink: link,
                                                    sourceTitle: obj.meta.title || obj.meta.description || obj.meta.link,
                                                    link: article.link,
                                                    origlink: article.origlink || '',
                                                    title: article.title,
                                                    date: new moment(article.date).format('x'),
                                                    pubDate: pdate,
                                                    realPubDate: article.pubDate || new moment().format('x'),
                                                    author: article.author || '',
                                                    source: JSON.stringify(article.source),
                                                    guid: article.guid,
                                                    categories: JSON.stringify(article.categories),
                                                    image: JSON.stringify(article.image),
                                                    summary: sanitizeHtml(article.summary, {
                                                      allowedTags: [ 'b', 'i', 'em', 'strong', 'a', 'p'],
                                                      allowedAttributes: {
                                                            'a': [ 'href' ]
                                                        }
                                                    }),
                                                    description: sanitizeHtml(article.description, {
                                                      allowedTags: [ 'b', 'i', 'em', 'strong', 'a', 'p'],
                                                      allowedAttributes: {
                                                            'a': [ 'href' ]
                                                        }
                                                    }),
                                                    type: type,
                                                    articleImage: articleImg,
                                                    id: uuid.v1()
                                                });
                                                links.push(article.link);
                                            }
                                        }
                                    });

                                    //console.time('create');

                                    if(obj.data.length > 0){
                                        update(params.link, url, params);
                                    }

                                }
                            });
                        }
                    }else{
                        console.log('no record');
                    }
                });
            }else{
                console.log('skipping');
            }
        });
    }
, 1000);*/

neo4j.API.send('MATCH (source:Source {id:{id}}) RETURN source', {
    params: {id: "ad4765b0-c0b1-11e5-9a1e-79c55c493251"}
}).then(function(data){
    if(data.length > 0){
        var url = "http://daringfireball.net";
        var str;
        var query;
        var params = {
            link:'',
            source:{},
            articles:[]
        }

        if(data[0].jsonURL){
            Parser.parseJSON(data[0].jsonURL, function(stream){
                var obj = JSON.parse(stream);
                if(obj && obj.items && obj.items.length > 0){
                    params.link = (obj.feed_url || url);
                    var link = (obj.home_page_url || url);
                    console.log(data[0].title);
                    params.source = data[0];
                    var links = [];
                    obj.items.forEach(function(article){
                        var descrip = article.description ? stripAds(article.description) : '';

                        var img = /<img[^>]+src="?([^"\s]+)"?[^>]*\/>/g.exec(descrip);
                        var articleImg;
                        var type = 'half';

                        if(img){
                            articleImg = '<section class="img" style="background-image:url(' + img[1] + ')"></section>';
                            type = 'half';
                        }else{
                            articleImg = '';
                            type = 'full';
                        }

                        if(article.url && article.title){
                            console.log(article.url);
                            var pdate = new moment(article.date_published);
                            if(!pdate.isValid() || pdate.format('x') > Date.now()){
                                pdate = new moment().format('x');
                            }else{
                                pdate = pdate.format('x');
                            }

                            if(links.indexOf(article.url) == -1){
                                params.articles.push({
                                    sourceLink: link,
                                    sourceTitle: obj.title || obj.description || obj.link,
                                    link: article.url,
                                    origlink: article.origlink || '',
                                    title: article.title,
                                    date: new moment(article.date).format('x'),
                                    pubDate: pdate,
                                    realPubDate: article.date_published || new moment().format('x'),
                                    dateModified: article.date_modified,
                                    author: article.author.name || '',
                                    source: JSON.stringify(article.source),
                                    guid: article.id,
                                    categories: JSON.stringify(article.categories),
                                    image: JSON.stringify(article.image),
                                    summary: sanitizeHtml(article.content_html, {
                                      allowedTags: [ 'b', 'i', 'em', 'strong', 'a', 'p'],
                                      allowedAttributes: {
                                            'a': [ 'href' ]
                                        }
                                    }),
                                    description: sanitizeHtml(article.content_html, {
                                      allowedTags: [ 'b', 'i', 'em', 'strong', 'a', 'p'],
                                      allowedAttributes: {
                                            'a': [ 'href' ]
                                        }
                                    }),
                                    type: type,
                                    articleImage: articleImg,
                                    id: uuid.v1()
                                });
                                links.push(article.link);
                            }
                        }
                    });

                    //console.time('create');

                    if(params.articles.length > 0){
                        update(params.link, url, params);
                    }

                }
            });
        }
    }else{
        console.log('no record');
    }
});

//PriorityQueue.clear();

process.on('SIGINT', function() {
  setTimeout(function() {
      process.exit(0);
  }, 300);
});

process.on('uncaughtException', function (err) {
  console.error(err.stack);
});
