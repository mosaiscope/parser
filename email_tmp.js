var apn = require('apn');
var neo4j = require('blklab-neo4j');
var Mailgun = require('mailgun-js');
var mailgun = new Mailgun({apiKey: 'key-de7f910e5ee2cbc222252fc63545cf59', domain: 'mail.mosaiscope.com'});
var fs = require('fs');
var SummaryTool = require('node-summary');
var striptags = require('striptags');
var moment = require('moment');

neo4j.Connect.options.host = '172.99.119.21';

function cutString(s, n){
    var cut= s.indexOf(' ', n);
    if(cut== -1) return s;
    return s.substring(0, cut)
}

//Me - 7f413760-2a32-11e5-946b-83c5199eaa2c
//John - a1f4a720-2a4a-11e5-903e-4def5ce658de
//Paul - bbfd94a0-2a4b-11e5-903e-4def5ce658de

exports = module.exports = {
    send: function(id, callback){
        var query = [
            'MATCH (user:Users {id: {user_id} }) MATCH (user)-[:SUMMARY {edition: user.edition}]-(article:Article)-[:HASARTICLE]-(source:Source) RETURN {email: user.email, articleTitle: article.title, articleText: article.description, articleLink: article.link, sourceTitle: source.title, articleImage: article.articleImage, author: article.author, guid: article.guid} AS data'
        ].join('\n');

        var struct = {};    
        var guids = [];
        
        neo4j.API.send(query, {params: {user_id: id} }).then(function(data){
            var email = data[0].email;
            var html = '';    
            
            var i = 0;            
            
            var next = function(){
                if(data.length > 0){
                    i++
                    var row = data.pop();
                    if(guids.indexOf(row.guid) == -1){
                        console.log(row.guid);
                        guids.push(row.guid);
                        var src = row.articleImage.replace(/<section class="img" style="background-image:url\(/, '').replace(/\)"><\/section>/, '');

                        if(src){
                            html += '<table class="row" style="height: 250px; width:auto; overflow: hidden; border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; position: relative; display: table; padding: 0;">';
                            html += '<tbody>';
                            html += '<tr style="vertical-align: top; text-align: left; padding: 0;" align="left">';
                            html += '<th class="small-12 large-4 columns first" style="height: auto !important; overflow: hidden; width: 310px !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; display: inline-block !important; margin: 0; padding: 0;" align="left">';
                            html += '<table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; padding: 0;">';
                            html += '<tr style="vertical-align: top; text-align: left; padding: 0;" align="left">';
                            html += '<th style="color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; margin: 0; padding: 0;" align="left">';
                            html += '<a href="' + row.articleLink + '" style="color: #000000; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; text-decoration: none; margin: 0; padding: 0;">';
                            html += '<div style="height: 250px; overflow: hidden; border-top-color: #000; border-top-width: 2px; border-top-style: solid; padding: 10px;">';
                            html += '<h3 style="font-size: 11px; height: 11px; overflow: hidden; color: inherit; font-family: Helvetica, Arial, sans-serif; font-weight: bold; text-align: left; line-height: 1.3; word-wrap: normal; text-transform: uppercase; margin: 0 0 10px; padding: 0;" align="left">';
                            html += '<b>SOURCE</b> ' + row.sourceTitle + '';
                            html += '</h3>';
                            html += '<h2 style="font-size: 16px; max-height: 40px; line-height: 20px; overflow: hidden; color: inherit; font-family: Helvetica, Arial, sans-serif; font-weight: bold; text-align: left; word-wrap: normal; text-transform: uppercase; margin: 0 0 10px; padding: 0;" align="left">';
                            html += '' + row.articleTitle + '';
                            html += '</h2>';
                            html += '<p style="font-size: 12px; height: 128px; overflow: hidden; line-height: 16px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; margin: 0 0 10px; padding: 0;" align="left">';
                            html += '' + cutString(striptags(row.articleText), 500);
                            html += '</p>';
                            html += '<p style="font-size: 11px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; margin: 0 0 10px; padding: 0;" align="left">';
                            html += '' + row.author + '';
                            html += '</p>';
                            html += '</div></a>';
                            html += '</th>';
                            html += '</tr>';
                            html += '</table>';
                            html += '</th>';
                            html += '<th class="small-12 large-4 columns last" style="height: auto !important; overflow: hidden; width: 295px !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; display: inline-block !important; margin: 0; padding: 0;" align="left">';
                            html += '<table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; padding: 0;">';
                            html += '<tr style="vertical-align: top; text-align: left; padding: 0;" align="left">';
                            html += '<th style="color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; margin: 0; padding: 0;" align="left">';
                            html += '<a href="' + row.articleLink + '" style="color: #000000; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; text-decoration: none; margin: 0; padding: 0;"></a>';
                            html += '<div style="height: 250px; overflow: hidden; border-top-width: 2px; border-top-color: #fff; border-top-style: solid;">';
                            html += '<img class="small-float-center img-max" src="' + row.articleImage.replace(/<section class="img" style="background-image:url\(/, '').replace(/\)"><\/section>/, '') + '" alt="' + row.articleTitle + '" width="100%" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto; max-width: 100%; clear: both; display: block; float: none !important; text-align: center !important; height: auto; margin: 0 auto; border: none;" align="none !important">';
                            html += '</div></a>';
                            html += '</th>';
                            html += '</tr>';
                            html += '</table>';
                            html += '</th>';
                            html += '</tr>';
                            html += '</tbody>';
                            html += '</table>';
                        }else{
                           html += '<table style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; padding: 0;">';
                            html += '<tr style="vertical-align: top; text-align: left; padding: 0;" align="left">';
                            html += '<th style="color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; font-size: 16px; margin: 0; padding: 0;" align="left">';
                            html += '<a href="' + row.articleLink + '" style="color: #000000; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; text-decoration: none; margin: 0; padding: 0;">';
                            html += '<div style="height: 250px; overflow: hidden; border-top-color: #000; border-top-width: 2px; border-top-style: solid; padding: 10px;">';
                            html += '<h3 style="font-size: 11px; height: 11px; overflow: hidden; color: inherit; font-family: Helvetica, Arial, sans-serif; font-weight: bold; text-align: left; line-height: 1.3; word-wrap: normal; text-transform: uppercase; margin: 0 0 10px; padding: 0;" align="left">';
                            html += '<b>SOURCE</b> ' + row.sourceTitle + '';
                            html += '</h3>';
                            html += '<h2 style="font-size: 16px; max-height: 40px; line-height: 20px; overflow: hidden; color: inherit; font-family: Helvetica, Arial, sans-serif; font-weight: bold; text-align: left; word-wrap: normal; text-transform: uppercase; margin: 0 0 10px; padding: 0;" align="left">';
                            html += '' + row.articleTitle + '';
                            html += '</h2>';
                            html += '<p style="font-size: 12px; height: 128px; overflow: hidden; line-height: 16px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; margin: 0 0 10px; padding: 0;" align="left">';
                            html += '' + cutString(striptags(row.articleText), 1000);
                            html += '</p>';
                            html += '<p style="font-size: 11px; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-weight: normal; text-align: left; line-height: 1.3; margin: 0 0 10px; padding: 0;" align="left">';
                            html += '' + row.author + '';
                            html += '</p>';
                            html += '</div></a>';
                            html += '</th>';
                            html += '</tr>';
                            html += '</table>';
                        }
                    }
                    next();
                }else{
                    try{
                        //var temp = fs.readFileSync('templates/template.html', "utf8");
                        var temp = fs.readFileSync('/var/www/parser/templates/template.html', "utf8");
                        var resp = temp.replace('{{data}}', html);
                        resp = resp.replace('{{date}}', moment().format('MMMM.Do.YYYY').toUpperCase());
                        var d = {
                            from: 'Mosaiscope<postmaster@mail.mosaiscope.com>',
                            to: email,
                            subject: 'Mosaiscope Daily Summary',
                            html: resp,
                            text: striptags(resp)
                        }
                        //fs.writeFileSync('templates/test_send.html', resp, "utf8");
                        mailgun.messages().send(d, function (err, body) {
                            if(err){
                                console.log(err);
                            }else{
                                console.log(body);
                            }
                            if(callback){
                                callback();
                            }
                        });

                    }catch(e){
                        console.log(e);
                    }        
                }
            }

            next();

        });
    }
}