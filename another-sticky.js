var neo4j = require('blklab-neo4j');
var moment = require('moment');
var fs = require("fs");

//neo4j.Connect.options.host = process.env.NODE_DB_HOST || '10.223.177.62';
neo4j.Connect.options.host = '104.130.241.94';


/*neo4j.API.send('MATCH (source:Source)-[s:SUBSCRIBES]-() RETURN COUNT(s) AS cnt, source.id, source.title ORDER BY cnt DESC', {ignoreColumns:false}).then(function(data){
    fs.writeFile('/var/www/parser/data/sources.json', JSON.stringify(data),  function(err) {
        console.log(err);
        console.log('done');
    });
});*/


var threshold = 1451865600000;
var type = process.argv[2];
var keep = [];

neo4j.API.send('MATCH (source:Source)-[s:SUBSCRIBES]-() RETURN COUNT(s) AS cnt, source.id, source.title ORDER BY cnt DESC', {ignoreColumns: false}).then(function(sources){
    function doSource(url){
        console.log(url);
        neo4j.API.send("MATCH (article)-[:HASARTICLE]-(:Source {id: {id} }) REMOVE article:Article RETURN article.id LIMIT 10000", {
                params:{
                    id: url
                },
                ignoreColumns:false
            }).then(function(data){
                neo4j.API.send("MATCH (article)-[:HASARTICLE]-(:Source {id: {id} }) SET article:Article RETURN article.id LIMIT 10000", {
                    params:{
                        id: url
                    },
                    ignoreColumns:false
                }).then(function(data){
                    if(sources.length > 0){
                        keep = [];
                        var s = sources.pop();
                        console.log("");
                        console.log('Starting ' + s['source.title']);
                        doSource(s['source.id']);
                    }else{
                        console.log('done');
                        process.exit(0);
                    }

                });
        });
    }
    sources = sources.reverse();
    var s = sources.pop();
    console.log('Starting ' + s['source.title']);
    doSource(s['source.id']);
});
