var PriorityQueue = require('./lib/queue.js');
var Poll = require('./lib/poll.js');
var neo4j = require('blklab-neo4j');
var moment = require('moment');

//neo4j.Connect.options.host = process.env.NODE_DB_HOST || '10.223.177.62';
neo4j.Connect.options.host = process.env.NODE_DB_ENV == 'DEV' ? '10.0.0.51' : '172.99.119.36';

console.log(neo4j.Connect.options.host);

var running = false;
var threshold = 600;
var sources = {};
var unusedSources = {};
var lastPopulated = 0;
var first = true;

var ready = function(previous, threshold) {
    if(!first){
        var elapsed = new moment().diff(previous, 'minutes');
        if(elapsed < threshold) {
             return false;
        }else{
            return true;
        }
    }else{
        return true;
    }
};

var shouldPopulate = function() {
    if(lastPopulated > 0){
        var elapsed = new moment().diff(lastPopulated, 'minutes');
        console.log('sources updated ' + new moment(lastPopulated).fromNow());
        if(elapsed < 120) {
             return false;
        }else{
            return true;
        }
    }else{
        return true;
    }
};

function run(){
    console.log('running...');
    Object.keys(sources).forEach(function(url){
        var source = sources[url];
        if(ready(source.stamp, 10)){
            console.log('updating ' + url);
            source.stamp = new moment();
            //var w = 2 - (1 * (parseFloat(source.cnt * 1.0)/100.0));
            var constant_factor =2;
            var clamp_adjustment = 2;
            var damping_factor = 2;
            var w = 2 - ((constant_factor / (parseFloat(source.cnt * 1.0) + clamp_adjustment))/100);
            PriorityQueue.remove(url);
            PriorityQueue.push(url, w);
        }
    });
    first = false;
}

function runUnused(){
    console.log('running unused...');
    Object.keys(unusedSources).forEach(function(url){
        var source = unusedSources[url];
        if(ready(source, threshold)){
            console.log('updating ' + url);
            unusedSources[url] = new moment();
            PriorityQueue.remove(url);
            PriorityQueue.push(url, 5.0);
        }
    });
}

function populate(){
    console.log('populating...');
    neo4j.API.send('MATCH (source:Source)-[s:SUBSCRIBES]-() RETURN COUNT(s) AS cnt, source ORDER BY cnt DESC', {ignoreColumns:false}).then(function(used){
        neo4j.API.send('MATCH (source:Source) WHERE NOT source-[:SUBSCRIBES]-() RETURN source.id').then(function(unused){
            used.forEach(function(source){
                sources[source.source.id + ':-:true'] = {
                    stamp: new moment(),
                    cnt: source.cnt
                };
            });

            unused.forEach(function(source){
                unusedSources[source + ':-:true'] = {
                    stamp: new moment(),
                    cnt: 0
                };
            });

            lastPopulated = new moment();

            run();
            runUnused();
        });
    });
}

PriorityQueue.clear();

Poll.start(
    function() {
        if(shouldPopulate()){
            populate();
        }else{
            run();
            runUnused();
        }
    }, 60000
);
