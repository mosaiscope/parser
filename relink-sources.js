var neo4j = require('blklab-neo4j');
var moment = require('moment');
var fs = require("fs");

//neo4j.Connect.options.host = process.env.NODE_DB_HOST || '10.223.177.62';
neo4j.Connect.options.host = '104.130.241.94';

var keep = [];
var run = function(){
    neo4j.API.send('MATCH n WHERE HAS(n.xmlurl) SET n:Source RETURN n.xmlurl', {ignoreColumns: false}).then(function(sources){
        console.log('done');
        process.exit(0);
    }, function(err){
        var id = err[0].message.match(/Node (\d+) already/)[1];
        var url = err[0].message.match(/=\[(.*)\]/)[1];
        console.log(id);
        console.log(url);
        
        neo4j.API.send('MATCH (n {xmlurl: {xmlurl} }) WHERE id(n) <> {id} DETACH DELETE n RETURN COUNT(n) AS cnt', {params: {id: parseInt(id), xmlurl: url}, ignoreColumns: false}).then(function(sources){
            console.log(sources[0].cnt + ' DELETED');
            console.log('');
            run();  
        }, function(err){
            console.log(err);
        });
        
    });
}
run();