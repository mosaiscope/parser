var apn = require('apn');
var neo4j = require('blklab-neo4j');
var Email = require('./email');

neo4j.Connect.options.host = '104.130.241.94';

var options = {
    production: false,
    cert: "keys/cert.pem",
    key: "keys/key.pem",
    passphrase: "T1meLo4d!"
};

var apnConnection = new apn.Connection(options);

apnConnection.on('error', function(err){
   console.log("error");  
    console.log(err);
    //process.exit(0);
})

apnConnection.on('socketError', function(err){
   console.log("socket error"); 
    console.log(err);
    //process.exit(0);
})

apnConnection.on('transmitted', function (notification, device) { 
    console.log("transmitted");
})

apnConnection.on('transmissionError', function(errorCode, notification, device) { 
    console.log(device)
    console.log(errorCode);
})


/*apnConnection.on('completed', function(){
    console.log('done');
    //process.exit(0);
});

apnConnection.on('disconnected', function(){
    console.log('dis');
    process.exit(0);
});*/

//Me - 7f413760-2a32-11e5-946b-83c5199eaa2c
//John - a1f4a720-2a4a-11e5-903e-4def5ce658de
//Paul - bbfd94a0-2a4b-11e5-903e-4def5ce658de

var query = [
    'MATCH (user:Users {id:"7f413760-2a32-11e5-946b-83c5199eaa2c"}) RETURN user.device AS device, user.id AS id'
].join('\n');

var struct = {};

neo4j.API.send(query, {ignoreColumns: false}).then(function(data){
    var token = data[0].device;
    var tokens = [];
    try{
        tokens = JSON.parse(token);
    }catch(e){
        tokens.push(token);
    }
    console.log(tokens);
    var note = new apn.Notification();
    note.badge = 1;
    note.sound = "ping.aiff";
    note.alert = "Your Mosiascope Daily Summary Is Available";
    note.payload = {'messageFrom': 'Mosaiscope'};

    apnConnection.pushNotification(note, tokens);   
    
    //Email.send(data[0].id);
});



/*process.on('SIGINT', function() {
  setTimeout(function() {
      process.exit(0);
  }, 300);
});

process.on('uncaughtException', function (err) {
  console.error(err.stack);
});*/