var neo4j = require('blklab-neo4j');
var moment = require('moment');
var fs = require("fs");

//neo4j.Connect.options.host = process.env.NODE_DB_HOST || '10.223.177.62';
neo4j.Connect.options.host = '104.130.241.94';

neo4j.API.send('MATCH (source:Source)-[s:SUBSCRIBES]-() RETURN COUNT(s) AS cnt, source.id, source.title ORDER BY cnt DESC', {ignoreColumns: false}).then(function(sources){
    console.log(sources.length);
    sources = sources.reverse();
    var next = function(source_id){            
        neo4j.API.send('MATCH (source:Source {id: {source_id} })-[:HASARTICLE]-(article:Article) WITH article.title AS title, collect(article) as all WITH head(all) as articles, tail(all) as leftover UNWIND leftover AS leftovers DETACH DELETE leftovers RETURN COUNT(leftovers) as cnt', {
            params:{
                source_id: source_id
            },
            ignoreColumns:false
        }).then(function(d){
            console.log(d[0].cnt + ' Deleted');
            if(sources.length > 0){
                var s = sources.pop();
                console.log();
                console.log('Starting ' + s['source.title']);
                next(s['source.id']);
            }else{
                console.log('done');
            }
        });
    }
    var s = sources.pop();
    console.log();
    console.log('Starting ' + s['source.title']);
    next(s['source.id']);
});