var neo4j = require('blklab-neo4j');
var moment = require('moment');
var request = require('request');
var apn = require('apn');
var Email = require('./email_tmp');
var Mailgun = require('mailgun-js');
var mailgun = new Mailgun({apiKey: 'key-de7f910e5ee2cbc222252fc63545cf59', domain: 'mail.mosaiscope.com'});
//var Syslog = require('node-syslog');

//Syslog.init("node-syslog", Syslog.LOG_PID | Syslog.LOG_ODELAY, Syslog.LOG_LOCAL0);

var isEMailing = false;

//neo4j.Connect.options.host = process.env.NODE_DB_HOST || '10.223.177.62';
neo4j.Connect.options.host = '172.99.119.21';
///var/www/parser/
var options = {
    production: true,
    cert: "keys/cert_production.pem",
    key: "keys/key_production.pem",
    passphrase: "T1meLo4d!"
};

var apnConnection = new apn.Connection(options);

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}


var auths = [
    "7f413760-2a32-11e5-946b-83c5199eaa2c",
    "b9f7b770-a061-11e6-b520-4d6feba5090c",
    "4bccaca0-7d18-11e5-bf1e-bf80b71f3949"
]
/*,
    "a1f4a720-2a4a-11e5-903e-4def5ce658de",
    "bbfd94a0-2a4b-11e5-903e-4def5ce658de",
    "2219de10-430b-11e5-81c6-4534766254a6",
    "4bccaca0-7d18-11e5-bf1e-bf80b71f3949"*/
//var auth_id = "7f413760-2a32-11e5-946b-83c5199eaa2c";
//Me - 7f413760-2a32-11e5-946b-83c5199eaa2c
//Me icloud - b9f7b770-a061-11e6-b520-4d6feba5090c
//Me gmail - 4bccaca0-7d18-11e5-bf1e-bf80b71f3949
//John - a1f4a720-2a4a-11e5-903e-4def5ce658de
//Paul - bbfd94a0-2a4b-11e5-903e-4def5ce658de
//Mike Falcone - 2219de10-430b-11e5-81c6-4534766254a6


apnConnection.on('error', function(err){
    //console.log("error");
    //console.log(err);
    //process.exit(0);
})

apnConnection.on('socketError', function(err){
    //console.log("socket error");
    //console.log(err);
    //process.exit(0);
})

apnConnection.on('transmitted', function (notification, device) {
    //Syslog.log(Syslog.LOG_INFO, "transmitted");
})

apnConnection.on('transmissionError', function(errorCode, notification, device) {
    //console.log(device)
    //console.log(errorCode);
})

apnConnection.on('completed', function(){
});

var done = function(){
    var d = {
        from: 'noreply@mail.mosaiscope.com',
        to: 'baird@lackner-buckingham.com',
        subject: 'Mosaiscope Daily Summary',
        html: "DONE"
    }

    mailgun.messages().send(d, function (err, body) {
        //Syslog.log(Syslog.LOG_INFO, 'actually done');
        process.exit(0);
    });
}

var check = function(){
    //console.log('checking');
    if((apnConnection.notificationBuffer.length > 0 || apnConnection.notificationsQueued) && !isEMailing){
        setTimeout(function(){
            check();
        }, 1000);
    }else{
        done();
    }
}

apnConnection.on("disconnected", function() {
    //console.log("Disconnected from APNS");
});

function collect(){
    var query = [
        'MATCH (user:Users {get_email:1}) WHERE NOT (user)-[:SUMMARY { day: {day}, month: {month}, year: {year} }]-(:Article) RETURN DISTINCT user.id'
    ].join('\n');

    var struct = {};

    neo4j.API.send(query, {params:{
        day: moment().date(),
        month: (moment().month() + 1),
        year: moment().year()
    }}).then(function(data){
        auths = data;
        start();
    });
}

function start(){
    console.log(auths.length);
    console.log("");
    if(auths.length > 0){
        var auth_id = auths.pop();
        var query = [
            'MATCH (user:Users {id: {user_id}}) SET user.edition = (toInt(user.edition) + 1) RETURN user AS edition, user.device AS device, user.get_email AS get_email'
        ].join('\n');

        var struct = {};
        neo4j.API.send(query, {
            params:{
                user_id: auth_id
            }
        }).then(function(data){
            if(data.length > 0){
                console.log(data[0].email);
                run(auth_id, data[0].edition, data[0].device, data[0].get_email);
            }else{
                start();
            }
        }, function(err){
            console.log(err);
            start();
        });
    }else{
        console.log('done');
        check();
        //process.exit(0);
    }
}
collect();

function run(id, edition, token, get_email){
    var query = [
        'MATCH (user:Users {id: {user_id}})-[s:SUBSCRIBES]-(source:Source)-[:HASARTICLE]-(article:Article)',
        'WHERE (toInt(article.pubDate) >= timestamp() - 86400000)',
        'RETURN s.slug AS slug, article.guid AS id, source.title AS real_title, article.link AS article_link, article.pubDate AS pubDate',
        'ORDER BY toInt(pubDate) DESC',
        'LIMIT 500'
    ].join('\n');

    var queryTopics = [
        'MATCH (user:Users {id: {user_id} })-[:SUBSCRIBESTOTOPIC]-(topic:Topic) ',
        'WITH DISTINCT(topic.slug) AS slug ',
        'MATCH (t:Topic {slug: slug})-[:ISTOPIC]-(s:Source) ',
        'WITH DISTINCT(s) AS source, slug ',
        'OPTIONAL MATCH (source)-[:HASARTICLE]-(a:Article) ',
        'WHERE (toInt(a.pubDate) >= timestamp() - 86400000) ',
        'RETURN slug, a.guid AS id, source.title AS real_title, a.link AS article_link, a.pubDate AS pubDate ',
        'ORDER BY toInt(pubDate) DESC ',
        'LIMIT 500'
    ].join('\n');

    var struct = {};

    neo4j.API.send(query, {
        params:{
            user_id: id
        },
        ignoreColumns: false
    }).then(function(data){
        neo4j.API.send(queryTopics, {
            params:{
                user_id: id
            },
            ignoreColumns: false
        }).then(function(dataTopics){

            data.forEach(function(row){
                if(!struct[row.slug]){
                    struct[row.slug] = [];
                }
                struct[row.slug].push(row);
            });

            dataTopics.forEach(function(row){
                if(!struct[row.slug]){
                    struct[row.slug] = [];
                }
                struct[row.slug].push(row);
            });

            var keepers = [];
            var store = {};
            var keys = Object.create(Object.keys(struct));

            var processResults = function(){
                var keys = Object.keys(store);
                if(keys.length > 0){
                    var trys = 0;
                    var keepFilling = function(idx){
                        trys++;
                        keys = shuffle(keys);
                        keys.forEach(function(key){
                            if(keepers.length < 10){
                                var guess = Math.floor(Math.random() * store[key].length)
                                console.log(key);
                                console.log(guess);
                                console.log(store[key].length)
                                console.log();

                                if(keepers.indexOf(store[key][guess].id) == -1){
                                    keepers.push(store[key][guess].id);
                                }
                            }
                        });
                        console.log(keepers.length);
                        if(keepers.length <= 10 && trys < 20){
                            console.log('going again');
                            keepFilling(idx + 1);
                        }else{
                            var query = [
                                "MATCH (source:Source)-[:HASARTICLE]-(articles:Article)",
                                "WHERE articles.guid IN {bunch}",
                                "MATCH (u:Users {id: {user_id}})",
                                "WITH u, articles",
                                "CREATE UNIQUE (u)-[:SUMMARY {edition: {edition}, day: {day}, month: {month}, year: {year} }]-(articles)",
                                "RETURN articles.guid AS id"
                            ].join('\n');
                            neo4j.API.send(query, {
                                params:{
                                    bunch: keepers,
                                    user_id: id,
                                    edition: edition,
                                    day: moment().date(),
                                    month: (moment().month() + 1),
                                    year: moment().year()
                                },ignoreColumns:false
                            }).then(function(data){
                                if(token != "" && data.length > 0){

                                    var note = new apn.Notification();
                                    note.badge = 1;
                                    note.sound = "ping.aiff";
                                    note.alert = "Your Mosiascope Daily Summary Is Available";
                                    note.payload = {'messageFrom': 'Mosaiscope'};

                                    var tokens = [];
                                    try{
                                        tokens = JSON.parse(token);
                                    }catch(e){
                                        tokens.push(token);
                                    }
                                    //console.log(tokens);
                                    if(tokens.length > 0){
                                        apnConnection.pushNotification(note, tokens);
                                    }
                                    if(get_email == 1){
                                        isEMailing = true
                                        Email.send(id, function(){
                                            isEMailing = false
                                            start();
                                        });
                                    }else{
                                        start();
                                    }
                                }else{
                                    if(get_email == 1 && data.length > 0){
                                        Email.send(id, function(){
                                            isEMailing = false
                                            start();
                                        });
                                    }else{
                                        start();
                                    }
                                }
                            });
                        }
                    }
                    keepFilling(0);

                }else{
                    console.log('skipped');
                    start();
                }
            }

            var next = function(){
                var key = keys.pop();
                var links = [];
                console.log(key);
                if(key && struct[key].length > 0){
                    var end = struct[key].length >= 250 ? 250 : struct[key].length
                    var s = struct[key].slice(0,250)
                    struct[key].forEach(function(row){
                        links.push(row.article_link);
                    });
                    //var url = "http://api.facebook.com/v2.0/restserver.php?method=links.getStats&format=json&urls=" + links.join(',');

                    //request(url, function (error, response, body) {
                        //if (!error && response.statusCode == 200) {
                            var resp = [];
                            links.forEach(function(r, idx){
                                resp.push({
                                    id: struct[key][idx].id,
                                    total_count: Math.floor(Math.random() * 600000) + 1
                                })
                            });

                            if(!Array.isArray(resp)){
                                resp = [resp];
                            }

                            var popular = resp.sort(function(a, b){
                                return a.total_count - b.total_count;
                            });

                            popular = popular.slice(0,100).reverse();

                            store[key] = popular;

                            if(keys.length > 0){
                                setTimeout(next, 1500);
                            }else{
                                processResults();
                            }
                        /*}else{
                            console.log(error);
                            console.log(response.statusCode);
                            if(keys.length > 0){
                                setTimeout(next, 1500);
                            }else{
                                processResults();
                            }
                        }*/
                    //})
                }else{
                    if(keys.length > 0){
                        setTimeout(next, 50);
                    }else{
                        processResults();
                    }
                }
            }
            if(keys.length > 0){
                setTimeout(next, 50);
            }else{
                processResults();
            }
        });
    });
}


process.on('SIGINT', function() {
  setTimeout(function() {
      //Syslog.log(Syslog.LOG_INFO, 'Error');
      process.exit(0);
  }, 300);
});

process.on('uncaughtException', function (err) {
    //Syslog.log(Syslog.LOG_INFO, 'Error');
    console.error(err.stack);
});
