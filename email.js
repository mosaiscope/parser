var apn = require('apn');
var neo4j = require('blklab-neo4j');
var Mailgun = require('mailgun-js');
var mailgun = new Mailgun({apiKey: 'key-de7f910e5ee2cbc222252fc63545cf59', domain: 'mail.mosaiscope.com'});
var fs = require('fs');
var SummaryTool = require('node-summary');
var striptags = require('striptags');
var moment = require('moment');

neo4j.Connect.options.host = '172.99.119.36';

//Me - 7f413760-2a32-11e5-946b-83c5199eaa2c
//John - a1f4a720-2a4a-11e5-903e-4def5ce658de
//Paul - bbfd94a0-2a4b-11e5-903e-4def5ce658de

exports = module.exports = {
    send: function(id, callback){
        var query = [
            'MATCH (user:Users {id: {user_id} }) MATCH (user)-[:SUMMARY {edition: user.edition}]-(article:Article)-[:HASARTICLE]-(source:Source) RETURN {email: user.email, articleTitle: article.title, articleText: article.description, articleLink: article.link, sourceTitle: source.title, articleImage: article.articleImage, author: article.author} AS data'
        ].join('\n');

        var struct = {};

        neo4j.API.send(query, {params: {user_id: id} }).then(function(data){
            var email = data[0].email;
            var html = '';    

            var next = function(){
                if(data.length > 0){
                    var row = data.pop();
                    var src = row.articleImage.replace(/<section class="img" style="background-image:url\(/, '').replace(/\)"><\/section>/, '');

                    console.log(src);
                    if(src){
                        html += '<table class="row" style="height:250px; overflow:hidden;">';
                        html += '<tbody>';
                        html += '<tr>';
                        html += '<th class="small-12 large-4 columns first" style="height:250px; overflow:hidden;">';
                        html += '<table>';
                        html += '<tr>';
                        html += '<th>';
                        html += '<a href="' + row.articleLink + '"><div style="height:250px; overflow:hidden; border-top:solid 2px #000; padding:10px;">';
                        html += '<h3 style="font-size:11px; height: 11px; overflow:hidden;"><b>SOURCE</b> ' + row.sourceTitle + '</h3>';
                        html += '<h2 style="font-size:16px; max-height: 40px; line-height: 20px; overflow:hidden;">' + row.articleTitle + '</h2>';
                        html += '<p style="font-size:12px; height:128px; overflow:hidden; line-height:16px;">' + striptags(row.articleText) + '</p>';
                        html += '<p style="font-size:11px">' + row.author + '</p></a>';
                        html += '</div>';
                        html += '</th>';
                        html += '</tr>';
                        html += '</table>';
                        html += '</th>';
                        html += '<th class="small-12 large-4 columns last" style="height:250px; overflow:hidden;">';
                        html += '<table>';
                        html += '<tr>';
                        html += '<th><a href="' + row.articleLink + '"><div style="height:250px; overflow:hidden; border-top:solid 2px #fff;"> <img class="small-float-center img-max" src="' + row.articleImage.replace(/<section class="img" style="background-image:url\(/, '').replace(/\)"><\/section>/, '') + '" alt="' + row.articleTitle + '" width="100%"></div></a> </th>';
                        html += '</tr>';
                        html += '</table>';
                        html += '</th>';
                        html += '</tr>';
                        html += '</tbody>';
                        html += '</table>';
                    }else{
                        html += '<table class="row" style="height:250px; overflow:hidden;">';
                        html += '<tbody>';
                        html += '<tr>';
                        html += '<th class="small-12 large-12 columns first last" style="height:250px; overflow:hidden;">';
                        html += '<table>';
                        html += '<tr>';
                        html += '<th>';
                        html += '<div style="height:250px; overflow:hidden; border-top:solid 2px #000; padding:10px;">';
                        html += '<h3 style="font-size:11px; height: 11px; overflow:hidden;"><b>SOURCE</b> ' + row.sourceTitle + '</h3>';
                        html += '<h2 style="font-size:16px; max-height: 40px; line-height: 20px; overflow:hidden;">' + row.articleTitle + '</h2>';
                        html += '<p style="font-size:12px; height:128px; overflow:hidden; line-height:16px;">' + striptags(row.articleText) + '</p>';
                        html += '<p style="font-size:11px">' + row.author + '</p>';
                        html += '</div>';
                        html += '</th>';
                        html += '</tr>';
                        html += '</table>';
                        html += '</th>';
                        html += '</tr>';
                        html += '</tbody>';
                        html += '</table>';
                    }
                    next();
                }else{
                    try{
                       
                        var temp = fs.readFileSync('/var/www/parser/templates/newsletter-2.html', "utf8");
                        var resp = temp.replace('{{data}}', html);
                        resp = resp.replace('{{date}}', moment().format('MMMM.Do.YYYY').toUpperCase());
                        var d = {
                            from: 'noreply@mail.mosaiscope.com',
                            to: email,
                            subject: 'Mosaiscope Daily Summary',
                            html: resp
                        }
                        mailgun.messages().send(d, function (err, body) {
                            if(err){
                                console.log(err);
                            }else{
                                console.log(body);
                            }
                            if(callback){
                                callback();
                            }
                        });

                    }catch(e){
                        console.log(e);
                    }        
                }
            }

            next();

        });
    }
}