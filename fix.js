var PriorityQueue = require('./lib/queue.js');
var Poll = require('./lib/poll.js');
var neo4j = require('blklab-neo4j');
var moment = require('moment');
var fs = require("fs");

//neo4j.Connect.options.host = process.env.NODE_DB_HOST || '10.223.177.62';
neo4j.Connect.options.host = '104.130.241.94';


/*neo4j.API.send('MATCH (source:Source)-[s:SUBSCRIBES]-() RETURN COUNT(s) AS cnt, source.id, source.title ORDER BY cnt DESC', {ignoreColumns:false}).then(function(data){
    fs.writeFile('/var/www/parser/data/sources.json', JSON.stringify(data),  function(err) {
        console.log(err);
        console.log('done');
    });
});*/


var threshold = 1451865600000;
var type = process.argv[2];
var keep = [];

if(type == "mail"){
    var query = [
        'MATCH (mail:Email)-[:HASMAIL]-(articles:Mail)',
        'RETURN articles SKIP 1500 LIMIT 500'
    ].join('\n');

    function runMail(){
        var next = function(){
            if(keep.length > 0){
                var id = keep.pop();
                neo4j.API.send('MATCH (e:Email)-[:HASMAIL]-(n {id: {id}}) DETACH DELETE n', {
                    params:{
                        id: id
                    },
                    ignoreColumns:false
                }).then(function(d){
                    process.stdout.write(keep.length + '->');
                    next();
                });
            }
        }
        //console.log(keep);
        next();
    }

    neo4j.API.send(query, {
            ignoreColumns:false
        }).then(function(data){
        var i = 0;
        data.forEach(function(a){
            var article = a.articles;
            var id = article['id'];

            neo4j.API.send('MATCH (n:Mail {id: {id}}) RETURN n', {
                params:{
                    id: id
                },
                ignoreColumns:false
            }).then(function(d){
                if(d.length == 0){
                    keep.push(id);
                }
                i++;
                if(i == data.length){
                    runMail();
                }
            });
        });
    });

}

if(!type){
    var query = [
        'MATCH (user:Users)-[s:SUBSCRIBES]-(source:Source {id: {id} })-[:HASARTICLE]-(article:Article)',
        'WITH user, (filter(x IN COLLECT(article) WHERE NOT (x:Article)-[:READ]-user)) AS article2, source',
        'UNWIND article2 AS articles',
        'RETURN DISTINCT articles, source AS meta LIMIT 1000'
    ].join('\n');

    neo4j.API.send('MATCH (source:Source)-[s:SUBSCRIBES]-() RETURN COUNT(s) AS cnt, source.id ORDER BY cnt DESC', {ignoreColumns: false}).then(function(sources){
        console.log(sources.length);
        //sources = sources.reverse();
        function run(source_id){
            var next = function(){
                if(keep.length > 0){
                    var id = keep.pop();
                    neo4j.API.send('MATCH (:Source {id: {source_id} })-[:HASARTICLE]-(n {id: {id}}) DETACH DELETE n', {
                        params:{
                            id: id,
                            source_id: source_id
                        },
                        ignoreColumns:false
                    }).then(function(d){
                        //console.log('Deleted ' + id);
                        process.stdout.write(keep.length + '->');
                        next();
                    });
                }else{
                    if(sources.length > 0){
                        keep = [];
                        var s = sources.pop();
                        console.log();
                        console.log('Starting ' + s['source.id']);
                        doSource(s['source.id']);
                    }else{
                        console.log('done');
                    }
                }
            }

            next();
        }

        function doSource(url){
            neo4j.API.send(query, {
                    params:{
                        id: url
                    },
                    ignoreColumns:false
                }).then(function(data){
                var i = 0;
                if(data.length > 0){
                    data.forEach(function(a){
                        var article = a.articles;
                        var id = article['id'];
                        neo4j.API.send('MATCH (:Source)-[:HASARTICLE]-(n:Article {id: {id}}) RETURN n', {
                            params:{
                                id: id
                            },
                            ignoreColumns:false
                        }).then(function(d){
                            if(d.length == 0){
                                keep.push(id);
                            }
                            i++;
                            if(i == data.length){
                                run(url);
                            }
                        });
                    });
                }else{
                    if(sources.length > 0){
                        keep = [];
                        var s = sources.pop();
                        console.log('Starting ' + s['source.id']);
                        doSource(s['source.id']);
                    }else{
                        console.log('done');
                        process.exit(0);
                    }
                }
            });
        }

        var s = sources.pop();
        console.log('Starting ' + s['source.id']);
        doSource(s['source.id']);
    });
}
