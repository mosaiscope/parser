var neo4j = require('blklab-neo4j');
var moment = require('moment');
var fs = require("fs");

//neo4j.Connect.options.host = process.env.NODE_DB_HOST || '10.223.177.62';
neo4j.Connect.options.host = '172.99.119.36';

var path = process.argv[2] || '';

var Backup = {
    saveUsers: function(callback){
        console.log('Saving Users');
        neo4j.API.send('MATCH (user:Users) RETURN user', {ignoreColumns: false}).then(function(data){
            fs.writeFileSync(path + 'data/users.json', JSON.stringify(data), "utf8");
            if(callback){
                callback();
            }
        });
    },

    saveFlagged: function(callback){
        console.log('Saving Flagged and Bookmarked');
        neo4j.API.send('MATCH (user:Users)-[r:FLAGGED|BOOKMARK]-(article:Article) RETURN {id: user.id, _id: id(user)} AS user, r, type(r) AS rel_type, article', {ignoreColumns: false}).then(function(data){
            fs.writeFileSync(path + 'data/favorites_flagged_articles.json', JSON.stringify(data), "utf8");
            if(callback){
                callback();
            }
        });
    },

    saveCollections: function(callback){
        console.log('Saving Collections');
        neo4j.API.send('MATCH (user:Users)-[hasCollection:HASCOLLECTION]-(collection:Collection)-[hasArticle:HASARTICLE]-(article:Article) RETURN {id: user.id, _id: id(user)} AS user, hasCollection, collection, hasArticle, article', {ignoreColumns: false}).then(function(data){
            fs.writeFileSync(path + 'data/collections_articles.json', JSON.stringify(data), "utf8");
            if(callback){
                callback();
            }
        });
    },

    saveSources: function(callback){
        console.log('Saving Sources');
        neo4j.API.send('MATCH (source:Source) RETURN source', {ignoreColumns: false}).then(function(data){
            fs.writeFileSync(path + 'data/sources.json', JSON.stringify(data), "utf8");
            if(callback){
                callback();
            }
        });
    },

    saveTopics: function(callback){
        console.log('Saving Topics');
        neo4j.API.send('MATCH (topic:Topic) RETURN topic', {ignoreColumns: false}).then(function(data){
            fs.writeFileSync(path + 'data/topics.json', JSON.stringify(data), "utf8");
            if(callback){
                callback();
            }
        });
    },

    run: function(){
        Backup.saveUsers(function(){
            Backup.saveSources(function(){
                Backup.saveTopics(function(){
                    Backup.saveCollections(function(){
                        Backup.saveFlagged(function(){
                            process.exit(0);
                        });
                    });
                });
            });
        })
    }
};

Backup.run();
