var sys = require('sys')
var exec = require('child_process').exec;
var path = '/root/.pm2/pm2.pid';
var fs = require('fs');
var Mailgun = require('mailgun-js');

function puts(error, stdout, stderr) {
    console.log(error);
    console.log(stdout);
    console.log(stderr);
    process.exit(0); 
}

function restartProcess(){
    var mailgun = new Mailgun({apiKey: 'key-de7f910e5ee2cbc222252fc63545cf59', domain: 'mail.mosaiscope.com'});

    var html = "Mosaiscope Updater and Parser have been restarted.";

    var data = {
        from: 'noreply@mail.mosaiscope.com',
        to: 'baird@lackner-buckingham.com',
        subject: 'Mosaiscope Parser: Restarted',
        html: html
    }

    mailgun.messages().send(data, function (err, body) {
        if (err) {
            console.log("got an error: ", err);
        } else {
            console.log(body);
        }

        exec("/root/.nvm/versions/node/v8.6.0/bin/pm2 resurrect", puts);
    });

}

function ok(){
    var mailgun = new Mailgun({apiKey: 'key-de7f910e5ee2cbc222252fc63545cf59', domain: 'mail.mosaiscope.com'});

    var html = "Mosaiscope Updater and Parser are online.";

    var data = {
        from: 'noreply@mail.mosaiscope.com',
        to: 'baird@lackner-buckingham.com',
        subject: 'Mosaiscope Parser: Online',
        html: html
    }

    mailgun.messages().send(data, function (err, body) {
        if (err) {
            console.log("got an error: ", err);
        } else {
            console.log(body);
        }
        process.exit(0);
    });
}

fs.readFile(path, function (err, pid) {
    if(pid){
        console.log(pid.toString());
        if(!require('is-running')(pid)){
            restartProcess();
        }else{
            console.log('still running');
            //ok();
            process.exit(0);
        }
    }else{
        restartProcess();
    }
});
